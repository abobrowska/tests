package ceneo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CeneoTest {
    private WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.ceneo.pl/");
    }

    @Test
    public void testKonto() {

        WebElement userIcon = driver.findElement(By.cssSelector("body > div.page-body > header > div.layout-wrapper.header__wrapper > div.header__user > div.my-account > a > span"));
        userIcon.click();

        WebElement signIn = driver.findElement(By.cssSelector("body > div.popup-overlay-container.popup-overlay--on-show-anim > div.js_popup-overlay.popup-overlay > div > div > div > div > div.login-options-menu__register > a"));
        signIn.click();

        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement email = driver.findElement(By.name("Email"));
        email.clear();
        email.sendKeys("gleiciane_mora@furnitt.com");

//        WebElement nickname = driver.findElement(By.id("Nickname"));
//        nickname.clear();
//        nickname.sendKeys("Gleiciane Mora");

        WebElement password = driver.findElement(By.name("Password"));
        password.clear();
        password.sendKeys("Qwerty1234!");

        WebElement checkboxFirst = driver.findElement(By.xpath("//*[@id=\"PersonalDataProcessingConsent\"]"));
        checkboxFirst.clear();
        checkboxFirst.click();

        WebElement captcha = driver.findElement(By.xpath("//*[@id=\"recaptcha-anchor\"]/div[1]"));
        captcha.clear();
        captcha.click();



    }
}
