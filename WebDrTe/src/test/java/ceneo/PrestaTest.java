package ceneo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PrestaTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");
    }

    @Test
    public void testKonto() {

        WebElement signIn = driver.findElement(By.cssSelector("#header > div.nav > div > div > nav > div.header_user_info > a"));
        signIn.click();

        WebElement emailCreate = driver.findElement(By.name("email_create"));
        emailCreate.clear();
        emailCreate.sendKeys("glen_mon@furnitt.com");

        WebElement submitCreate = driver.findElement(By.id("SubmitCreate"));
        submitCreate.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement gender = driver.findElement(By.cssSelector("#id_gender2"));
        gender.click();

        WebElement firstName = driver.findElement(By.name("customer_firstname"));
        firstName.clear();
        firstName.sendKeys("Glen");

        WebElement lastName = driver.findElement(By.name("customer_lastname"));
        lastName.clear();
        lastName.sendKeys("Mon");

        WebElement password = driver.findElement(By.name("passwd"));
        password.clear();
        password.sendKeys("Qwerty123!");

        driver.findElement(By.cssSelector("#days")).sendKeys("10");

        driver.findElement(By.cssSelector("#months")).sendKeys("April");

        driver.findElement(By.cssSelector("#years")).sendKeys("1996");

        WebElement company = driver.findElement(By.id("company"));
        company.clear();
        company.sendKeys("Qwerty");

        WebElement address = driver.findElement(By.id("address1"));
        address.clear();
        address.sendKeys("Long Street 2");

        WebElement city = driver.findElement(By.id("city"));
        city.clear();
        city.sendKeys("Qwerty City");

        driver.findElement(By.cssSelector("#id_state")).sendKeys("Arizona");

        WebElement mobilePhone = driver.findElement(By.id("phone_mobile"));
        mobilePhone.clear();
        mobilePhone.sendKeys("000 000 000");


    }

    private class Select {
        public Select(WebElement birthDay) {
        }
    }
}
