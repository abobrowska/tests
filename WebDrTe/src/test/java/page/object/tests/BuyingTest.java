package page.object.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.BasePage;
import pages.BuyingPage;
import pages.LoginPage;

public class BuyingTest {
    private static WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
    }
    @Test
    public void testBuyingProduct() {
        BuyingPage buyingPage = new BuyingPage(driver);
        buyingPage.loginAs("gleiciane_mora@furnitt.com", "Qwerty123!");
        Assert.assertEquals("Gleiciane Mora", buyingPage.getLoggedUsername());
    }
    @After
    public void tearDown() {
        driver.quit();
    }
}
