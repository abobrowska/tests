package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BuyingPage extends BasePage {
    @FindBy(name = "email")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    WebElement signInButton;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
    WebElement clickWomanCategory;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[1]/div/div[1]/div/a[1]/img")
    WebElement clickImage;

    @FindBy(xpath = "//*[@id=\"quantity_wanted_p\"]/a[2]")
    WebElement clickQuantityFirst;

    @FindBy(xpath = "//*[@id=\"quantity_wanted_p\"]/a[2]")
    WebElement clickQuantitySecond;

    @FindBy(xpath = "//*[@id=\"attributes\"]/fieldset[1]/div")
    WebElement choseSize;

    @FindBy(name = "Blue")
    WebElement chooseColor;

    @FindBy(name="Submit")
    WebElement submitProduct;

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a")
    WebElement proceedToCheckout;

    @FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[1]")
    WebElement summaryCheckout;

    @FindBy(name = "processAddress")
    WebElement processAddress;

    @FindBy(name = "cgv")
    WebElement tesrmsOfService;

    @FindBy(name = "processCarrier")
    WebElement processCarrier;

    public BuyingPage(WebDriver driver) {
        super(driver);
    }
    public void buiyngProduct(){
        click(loginInput);
        click(passwordInput);
        click(signInButton);
        click(clickWomanCategory);
        click(clickImage);
        click(clickQuantityFirst);
        click(clickQuantitySecond);
        click(choseSize);
        click(chooseColor);
        click(submitProduct);
        click(proceedToCheckout);
        click(summaryCheckout);
        click(processAddress);
        click(tesrmsOfService);
        click(processCarrier);

    }
    public String getLoggedUsername() {
        WebElement userName = driver.findElement(By.cssSelector("#header > div.nav > div > div > nav > div:nth-child(1) > a"));
        return userName.getText();
    }
}
