package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class LoginPage extends BasePage {

    @FindBy(name = "email")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    WebElement signInButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void loginAs(String email, String password) {
//        WebElement loginInput = driver.findElement(By.name("email_create"));
        click(loginInput);  //z waitem z BasePage (zamiast: loginInput.click();)
        loginInput.clear();
        loginInput.sendKeys(email);

//        WebElement passwordInput = driver.findElement(By.name("passwd"));
        click(passwordInput);
        passwordInput.clear();
        passwordInput.sendKeys(password);

//        WebElement signInButton = driver.findElement(By.id("SubmitLogin"));
        click(signInButton);
    }

    public String getLoggedUsername() {
        WebElement userName = driver.findElement(By.cssSelector("#header > div.nav > div > div > nav > div:nth-child(1) > a"));
        return userName.getText();
    }
}
